﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace MdDAdventurer.Tests
{
    [TestFixture]
    class CarteTest
    {

        private static readonly List<string> montagneList = new List<string> { "M-1-2", "M-0-0" };

        [TestCase("3", "4")]
        public void Constructor_expect_value(string width, string height)
        {
            Carte carte = new Carte(width, height);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(carte.Width, 3);
                Assert.AreEqual(carte.Height, 4);
            });
        }


        [TestCase("T", "2")]
        [TestCase("2", "T")]
        [TestCase("R", "T")]
        public void ConstructorStringNotNumerical_expect_Exception(string width, string height)
        {
            int exceptions = 0;
            try
            {
                Carte carte = new Carte(width, height);
            }
            catch (FormatException)
            {
                exceptions++;
            }

            Assert.AreEqual(1, exceptions);
        }

        [TestCase]
        public void PopulateMontagnes_Expect_ValueInMontagne()
        {
            Carte carte = new Carte("10", "10");
            carte.PopulateMontagnes(new List<string> { "M-1-2", "M-0-0" });
            Assert.Multiple(() =>
            {
                Assert.AreEqual(carte.Montagnes[0].Position.Horizontal, 1);
                Assert.AreEqual(carte.Montagnes[0].Position.Vertical, 2);
                Assert.AreEqual(carte.Montagnes[1].Position.Horizontal, 0);
                Assert.AreEqual(carte.Montagnes[1].Position.Vertical, 0);
            });
        }

        [TestCase]
        public void PopulateNoMontagnes_Expect_EmptyInMontagne()
        {
            Carte carte = new Carte("10", "10");
            carte.PopulateMontagnes(new List<string>());
            Assert.That(carte.Montagnes, Is.All.Empty);
        }

        [TestCase]
        public void PopulateTresor_Expect_ValueInTresor()
        {
            Carte carte = new Carte("10", "10");
            carte.PopulateTresor(new List<string> { "T-1-2-4", "T-0-0-5" });
            Assert.Multiple(() =>
            {
                Assert.AreEqual(carte.Tresors[0].Position.Horizontal, 1);
                Assert.AreEqual(carte.Tresors[0].Position.Vertical, 2);
                Assert.AreEqual(carte.Tresors[0].TresorCount, 4);
                Assert.AreEqual(carte.Tresors[1].Position.Horizontal, 0);
                Assert.AreEqual(carte.Tresors[1].Position.Vertical, 0);
                Assert.AreEqual(carte.Tresors[1].TresorCount, 5);
            });
        }

        [TestCase]
        public void PopulateNoTresor_Expect_EmptyInTresor()
        {
            Carte carte = new Carte("10", "10");
            carte.PopulateTresor(new List<string>());
            Assert.That(carte.Tresors, Is.All.Empty);
        }

        [TestCase]
        public void PopulateTresorCountIsNotNumerical_Expect_Exception()
        {
            int exceptions = 0;
            Carte carte = new Carte("10", "10");
            try
            {
                carte.PopulateTresor(new List<string> { "T-1-2-E", "T-0-0-5" });
            }
            catch (FormatException)
            {
                exceptions++;
            }

            Assert.AreEqual(1, exceptions);
        }
    }
}
