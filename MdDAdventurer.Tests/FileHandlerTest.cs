﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace MdDAdventurer.Tests
{
    [TestFixture]
    class FileHandlerTest
    {
        [TestCase(@".\TestFileInput\EntryFile.txt")]
        public void Constructor_WithGoodPath_Expect_InitFileLineNotNull(string path)
        {
            FileHandler fileHandler = new FileHandler(path);
            Assert.That(fileHandler.InitFileLines, Is.All.Not.Null);
        }

        [TestCase("fef")]
        public void Constructor_WithWrongPath_Expect_Error(string path)
        {
            int exceptions = 0;
            try
            {
                FileHandler fileHandler = new FileHandler(path);
            }
            catch (System.IO.FileNotFoundException)
            {
                exceptions++;
            }

            Assert.AreEqual(1, exceptions);
        }

        [TestCase(@".\TestFileInput\EntryFile.txt")]
        public void InitCarte_Simple_Expect_Carte(string path)
        {
            FileHandler fileHandler = new FileHandler(path);
            Carte carte = fileHandler.InitCarte();
            Assert.That(carte, Is.Not.Null);

        }

        [TestCase(@".\TestFileInput\EntryFile.txt")]
        public void InitCarte_SimpleWithMoutains_Expect_CarteWithMoutain(string path)
        {
            FileHandler fileHandler = new FileHandler(path);
            Carte carte = fileHandler.InitCarte();
            Assert.That(carte.Montagnes, Is.All.Not.Null);

        }

        [TestCase(@".\TestFileInput\EntryFile.txt")]
        public void InitCarte_SimpleWithTresor_Expect_CarteWithTresor(string path)
        {
            FileHandler fileHandler = new FileHandler(path);
            Carte carte = fileHandler.InitCarte();
            Assert.That(carte.Tresors, Is.All.Not.Null);
        }

        [TestCase(@".\TestFileInput\EntryFile.txt")]
        public void InitAventurier_Standard_Expect_ListOfAventurier(string path)
        {
            FileHandler fileHandler = new FileHandler(path);
            List<Aventurier> aventuriers = fileHandler.InitAventurier();
            Assert.That(aventuriers, Is.All.Not.Null);
        }

        [TestCase(@".\TestFileInput\EntryFileWrongAventurierWrongDirection.txt")]
        [TestCase(@".\TestFileInput\EntryFileWrongAventurierWrongMvt.txt")]
        public void InitAventurier_WrongMouvementOrDirection_Expect_Exception(string path)
        {

            FileHandler fileHandler = new FileHandler(path);
            int exceptions = 0;
            try
            {
                List<Aventurier> aventuriers = fileHandler.InitAventurier();
            }
            catch (ArgumentException)
            {
                exceptions++;
            }
            Assert.AreEqual(1, exceptions);
        }

        [TestCase(@".\TestFileInput\EntryFileWrongAventurierNoAventurier.txt")]
        public void InitAventurier_NoAventurier_Expect_ListEmpty(string path)
        {
            FileHandler fileHandler = new FileHandler(path);
            List<Aventurier> aventuriers = fileHandler.InitAventurier();
            Assert.That(aventuriers, Is.Empty);
        }




    }
}
