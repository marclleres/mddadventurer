﻿namespace MdDAdventurer
{
    public class Tresor : ICase
    {
        public Coords Position { get; set; }
        public int TresorCount { get; set; }

        public Tresor(Coords position, int tresorCount)
        {
            Position = position;
            TresorCount = tresorCount;
        }

    }
}
