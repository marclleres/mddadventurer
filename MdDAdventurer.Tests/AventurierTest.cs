﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MdDAdventurer.Tests
{
    [TestFixture]
    class AventurierTest
    {
        [TestCase(1, 2, "Judith", Orientation.S, Mouvement.A)]
        public void Constructor_expect_value(int hori, int vert, string name, Orientation ori, Mouvement mvt)
        {

            Aventurier aventurier = new Aventurier(new Coords(hori, vert), name, ori, new List<Mouvement>() { mvt });

            Assert.Multiple(() =>
            {
            Assert.AreEqual(aventurier.Position.Horizontal, 1);
            Assert.AreEqual(aventurier.Position.Vertical, 2);
            Assert.AreEqual(aventurier.Name, "Judith");
            Assert.AreEqual(aventurier.Orientation, Orientation.S);
            Assert.AreEqual(aventurier.MvtSequence, new List<Mouvement>() { Mouvement.A });
            });
        }

        [TestCase]
        public void GetNextPosition_Standard_expect_NextPosition()
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", Orientation.S, new List<Mouvement>() { Mouvement.A });
            Carte carte = new Carte("10", "10");
            aventurier.GetNextPosition(0, carte);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(aventurier.Position.Horizontal, 0);
                Assert.AreEqual(aventurier.Position.Vertical, 1);
            });
        }

        [TestCase(Orientation.N)]
        [TestCase(Orientation.S)]
        [TestCase(Orientation.E)]
        [TestCase(Orientation.O)]
        public void GetNextPosition_OutOfMap_expect_SamePosition(Orientation ori)
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", ori, new List<Mouvement>() { Mouvement.A });
            Carte carte = new Carte("0", "0");
            aventurier.GetNextPosition(0, carte);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(aventurier.Position.Horizontal, 0);
                Assert.AreEqual(aventurier.Position.Vertical, 0);
            });
        }

        [TestCase]
        public void GetNextPosition_AtoMontagne_expect_SamePosition()
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", Orientation.S, new List<Mouvement>() { Mouvement.A });
            Carte carte = new Carte("10", "10");
            carte.PopulateMontagnes(new List<string>() { "M-0-1" });

            aventurier.GetNextPosition(0, carte);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(aventurier.Position.Horizontal, 0);
                Assert.AreEqual(aventurier.Position.Vertical, 0);
            });
        }

        [TestCase]
        public void GetNextPosition_AtoTresor_expect_NextPosition_MoreTresorOnAven_LessTresorOnMap()
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", Orientation.S, new List<Mouvement>() { Mouvement.A });
            Carte carte = new Carte("10", "10");
            carte.PopulateTresor(new List<string>() { "T-0-1-3" });

            aventurier.GetNextPosition(0, carte);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(aventurier.Position.Horizontal, 0);
                Assert.AreEqual(aventurier.Position.Vertical, 1);
                Assert.AreEqual(aventurier.TresorCount, 1);
                Assert.AreEqual(carte.Tresors[0].TresorCount, 2) ;
            });
        }

        [TestCase(Orientation.N)]
        public void GetNextPosition_ChangeOriND_Expect_oriE(Orientation ori)
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", ori, new List<Mouvement>() { Mouvement.D });
            Carte carte = new Carte("10", "10");
            aventurier.GetNextPosition(0, carte);

            Assert.AreEqual(aventurier.Orientation, Orientation.E);
        }

        [TestCase(Orientation.N)]
        public void GetNextPosition_ChangeOriNG_Expect_oriO(Orientation ori)
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", ori, new List<Mouvement>() { Mouvement.G });
            Carte carte = new Carte("10", "10");
            aventurier.GetNextPosition(0, carte);

            Assert.AreEqual(aventurier.Orientation, Orientation.O);
        }


        [TestCase(Orientation.S)]
        public void GetNextPosition_ChangeOriSD_Expect_oriO(Orientation ori)
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", ori, new List<Mouvement>() { Mouvement.D });
            Carte carte = new Carte("10", "10");
            aventurier.GetNextPosition(0, carte);

            Assert.AreEqual(aventurier.Orientation, Orientation.O);
        }

        [TestCase(Orientation.S)]
        public void GetNextPosition_ChangeOriSG_Expect_oriE(Orientation ori)
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", ori, new List<Mouvement>() { Mouvement.G });
            Carte carte = new Carte("10", "10");
            aventurier.GetNextPosition(0, carte);

            Assert.AreEqual(aventurier.Orientation, Orientation.E);
        }

        [TestCase(Orientation.E)]
        public void GetNextPosition_ChangeOriED_Expect_oriS(Orientation ori)
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", ori, new List<Mouvement>() { Mouvement.D });
            Carte carte = new Carte("10", "10");
            aventurier.GetNextPosition(0, carte);

            Assert.AreEqual(aventurier.Orientation, Orientation.S);
        }

        [TestCase(Orientation.E)]
        public void GetNextPosition_ChangeOriEG_Expect_oriN(Orientation ori)
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", ori, new List<Mouvement>() { Mouvement.G });
            Carte carte = new Carte("10", "10");
            aventurier.GetNextPosition(0, carte);

            Assert.AreEqual(aventurier.Orientation, Orientation.N);
        }

        [TestCase(Orientation.O)]
        public void GetNextPosition_ChangeOriOD_Expect_oriN(Orientation ori)
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", ori, new List<Mouvement>() { Mouvement.D });
            Carte carte = new Carte("10", "10");
            aventurier.GetNextPosition(0, carte);

            Assert.AreEqual(aventurier.Orientation, Orientation.N);
        }

        [TestCase(Orientation.O)]
        public void GetNextPosition_ChangeOriOG_Expect_oriSN(Orientation ori)
        {
            Aventurier aventurier = new Aventurier(new Coords(0, 0), "Judith", ori, new List<Mouvement>() { Mouvement.G });
            Carte carte = new Carte("10", "10");
            aventurier.GetNextPosition(0, carte);

            Assert.AreEqual(aventurier.Orientation, Orientation.S);
        }

    }
}
