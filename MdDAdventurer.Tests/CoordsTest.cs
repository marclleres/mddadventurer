﻿using NUnit.Framework;
using System;

namespace MdDAdventurer.Tests
{
    [TestFixture]
    class CoordsTest
    {
        [TestCase(1, 2)]
        public void ConstructorInt_expect_value(int horizontal, int vertical)
        {
            Coords coords = new Coords(horizontal, vertical);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(coords.Horizontal, 1);
                Assert.AreEqual(coords.Vertical, 2);
            });
        }

        [TestCase("1", "2")]
        public void ConstructorString_expect_value(string horizontal, string vertical)
        {
            Coords coords = new Coords(horizontal, vertical);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(coords.Horizontal, 1);
                Assert.AreEqual(coords.Vertical, 2);
            });
        }

        [TestCase("T", "2")]
        [TestCase("2", "T")]
        [TestCase("R", "T")]
        public void ConstructorStringNotNumerical_expect_Exception(string horizontal, string vertical)
        {
            int exceptions = 0;
            try
            {
                Coords coords = new Coords(horizontal, vertical);
            }
            catch (FormatException)
            {
                exceptions++;
            }

            Assert.AreEqual(1, exceptions);
        }

        [TestCase]
        public void Compare_SameValue_Expect_true()
        {
            Coords coords = new Coords(1, 2);
            Coords Compare = new Coords(1, 2);

            Assert.IsTrue(coords.Compare(Compare));
        }

        [TestCase(1, 2)]
        [TestCase(3, 4)]
        public void Compare_DifferentValue_Expect_False(int horizontal, int vertical)
        {
            Coords coords = new Coords(horizontal, vertical);
            Coords Compare = new Coords(1, 4);

            Assert.IsFalse(coords.Compare(Compare));
        }



    }
}
