﻿namespace MdDAdventurer
{
    public interface ICase
    {
        Coords Position { get; set; }
    }
}
