﻿namespace MdDAdventurer
{
    public enum Orientation
    {
        S = 0,
        E = 1,
        N = 2,
        O = 3
    }
}
