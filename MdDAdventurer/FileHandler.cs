﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MdDAdventurer
{
    public class FileHandler
    {
        public List<string> InitFileLines { get; private set; } = new List<string>();

        public FileHandler(string path)
        {
            //StreamReader reader = File.OpenText(path);
            using (StreamReader rdr = new StreamReader(path))
            {
                string line;
                while ((line = rdr.ReadLine()) != null)
                {
                    InitFileLines.Add(line.Replace(" ", ""));
                }
            }

        }

        /// <summary>
        /// Init the carte and it moutains and tresor
        /// </summary>
        /// <returns></returns>
        public Carte InitCarte()
        {
            try
            {
                string[] size = InitFileLines.Where(l => l.StartsWith("C")).FirstOrDefault().Split('-');

                if (size == null)
                    throw new ArgumentNullException("No map size");


                Carte carte = new Carte(size[1], size[2]);

                List<string> listMontagnes = InitFileLines.Where(l => l.StartsWith("M")).ToList();
                List<string> listTresors = InitFileLines.Where(l => l.StartsWith("T")).ToList();

                if (listMontagnes != null)
                    carte.PopulateMontagnes(listMontagnes);

                if (listTresors != null)
                    carte.PopulateTresor(listTresors);
                return carte;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Init all the adventurer
        /// </summary>
        /// <returns></returns>
        public List<Aventurier> InitAventurier()
        {
            try
            {
                List<string> ListAventurierString = InitFileLines.Where(l => l.StartsWith("A")).ToList();

                List<Aventurier> Aventuriers = new List<Aventurier>();
                foreach (string aventurierString in ListAventurierString)
                {
                    string[] splitline = aventurierString.Split('-');

                    Coords coord = new Coords(splitline[2], splitline[3]);

                    List<Mouvement> mvts = new List<Mouvement>();

                    //Create a list of mouvement
                    foreach (var mvt in splitline[5].ToArray())
                    {
                        //If mouvement is not ADG, throw an error
                        if (!Enum.TryParse(mvt.ToString(), out Mouvement mouvement))
                            throw new ArgumentException("one of " + splitline[1] + "'s Mouvement is not A, D or G");
                        mvts.Add(mouvement);
                    }

                    //If Orientation is not NSEO, throw an error
                    if (!Enum.TryParse(splitline[4], out Orientation orientation))
                        throw new ArgumentException("Orientation of " + splitline[1] + " is not N, S, E or O");

                    //Create Aventurier
                    Aventurier aventurier = new Aventurier(coord, splitline[1], orientation, mvts);


                    Aventuriers.Add(aventurier);
                }

                return Aventuriers;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Create Output to display Result
        /// </summary>
        /// <param name="carte"></param>
        /// <param name="aventuriers"></param>
        /// <param name="path"></param>
        public void DisplayResult(Carte carte, List<Aventurier> aventuriers, string path)
        {
            StringBuilder output = new StringBuilder();
            string separator = " - ";

            output.AppendLine("C" + separator + carte.Width + separator + carte.Height);

            foreach (Montagne montagne in carte.Montagnes)
            {
                output.AppendLine("M" + separator + montagne.Position.Horizontal + separator + montagne.Position.Vertical);
            }

            output.AppendLine("# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants} ");

            foreach (Tresor tresor in carte.Tresors)
            {
                if(tresor.TresorCount > 0)
                    output.AppendLine("T" + separator + tresor.Position.Horizontal + separator + tresor.Position.Vertical + separator + tresor.TresorCount);
            }

            output.AppendLine("# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation } - {Nb.trésors ramassés}");

            foreach (Aventurier aventurier in aventuriers)
            {
                output.AppendLine("A" + separator + aventurier.Name + separator + aventurier.Position.Horizontal + separator + aventurier.Position.Vertical
                                    + separator + aventurier.Orientation + separator + aventurier.TresorCount);
            }

            using (StreamWriter wtr = new StreamWriter(path))
            {
                wtr.Write(output);
            }

        }
    }
}
