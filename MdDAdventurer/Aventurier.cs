﻿using System.Collections.Generic;
using System.Linq;

namespace MdDAdventurer
{
    public class Aventurier : ICase
    {
        public Coords Position { get; set; }
        public int TresorCount { get; set; }
        public string Name { get; set; }
        public Orientation Orientation { get; set; }
        public List<Mouvement> MvtSequence { get; set; }

        public Aventurier(Coords position, string name, Orientation orentiation, List<Mouvement> mvtSequence)
        {
            Position = position;
            TresorCount = 0;
            Name = name;
            Orientation = orentiation;
            MvtSequence = mvtSequence;
        }

        
        /// <summary>
        /// Ge Next Position Given the Turn of game and the card
        /// </summary>
        /// <param name="turn"></param>
        /// <param name="carte"></param>
        /// <returns></returns>
        public Coords GetNextPosition(int turn , Carte carte)
        {
            Coords nextPosition = new Coords();

            if (MvtSequence[turn] != Mouvement.A)
            {
                Orientation = NewOrientation(MvtSequence[turn]);
                return Position;
            }
            else if (MvtSequence[turn] == Mouvement.A)
            {
                nextPosition = Move();
                // CHeck if there is moutain on the next position or if the player goes out of the map
                if (carte.Montagnes.Exists(m => m.Position.Compare(nextPosition)) || nextPosition.Horizontal < 0 || nextPosition.Vertical < 0 || nextPosition.Horizontal > carte.Height || nextPosition.Vertical > carte.Width)
                {
                    nextPosition = Position;
                }
                CheckAndAddTresor(nextPosition, carte);
            }

            Position = nextPosition;
            return Position;
        }

        /// <summary>
        /// Get Orientiation given Direction and currrent Orientation
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        private Orientation NewOrientation(Mouvement direction)
        {
            int indexDir = (int)Orientation;

            switch (direction)
            { 
                case Mouvement.D:
                    indexDir--;
                    break;
                case Mouvement.G:
                    indexDir++;
                    break;
                default:
                    break;
            }

            switch (indexDir)
            {
                case -1:
                    indexDir = 3;
                    break;
                case 4:
                    indexDir = 0;
                    break;
            }

            return (Orientation)indexDir;
        }

        /// <summary>
        /// Move, Given the Current Orientation
        /// </summary>
        /// <returns></returns>
        private Coords Move()
        {
            Coords nextCoords = new Coords(Position.Horizontal, Position.Vertical);
            switch (Orientation)
            {
                case Orientation.S:
                    nextCoords.Vertical++;
                    break;
                case Orientation.E:
                    nextCoords.Horizontal++;
                    break;
                case Orientation.N:
                    nextCoords.Vertical--;
                    break;
                case Orientation.O:
                    nextCoords.Horizontal--;
                    break;
            }

            return nextCoords;
        }

        /// <summary>
        /// Check if there is a tresor and increment Adventurer Tresor and decremente Carte Tresor
        /// </summary>
        /// <param name="nextPosition"></param>
        /// <param name="carte"></param>
        private void CheckAndAddTresor(Coords nextPosition, Carte carte)
        {
            Tresor tresorCarte = carte.Tresors.Where(t => t.Position.Compare(nextPosition)).FirstOrDefault();
            if (tresorCarte != null && tresorCarte.TresorCount > 0)
            {
                TresorCount++;
                tresorCarte.TresorCount--;
            }
        }

    }
}
