﻿namespace MdDAdventurer
{
    public class Montagne : ICase
    {
        public Coords Position { get; set; }

        public Montagne(Coords position)
        {
            Position = position;
        }
    }
}
