﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace MdDAdventurer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string folderPath = ConfigurationManager.AppSettings["FolderPath"];
                FileHandler fileHandler = new FileHandler(folderPath + ConfigurationManager.AppSettings["EntryFile"]);
                Carte carte = fileHandler.InitCarte();

                List<Aventurier> aventurier = fileHandler.InitAventurier();

                for (int i = 0; i < aventurier[0].MvtSequence.Count; i++)
                {
                    aventurier[0].GetNextPosition(i, carte);
                }

                fileHandler.DisplayResult(carte, aventurier, folderPath + ConfigurationManager.AppSettings["OutputFile"]);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
            

        }
    }
}
