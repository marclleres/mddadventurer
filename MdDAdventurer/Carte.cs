﻿using System;
using System.Collections.Generic;

namespace MdDAdventurer
{
    public class Carte
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public List<Montagne> Montagnes { get; private set; } = new List<Montagne>();
        public List<Tresor> Tresors { get; private set; } = new List<Tresor>();


        public Carte(string width, string height)
        {
            try
            {
                if (!Int32.TryParse(width, out int w) || !Int32.TryParse(height, out int h))
                {
                    throw new FormatException("Carte size is not numerical");
                }
                Width = w;
                Height = h;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Populate the card with Montagnes, given a list of Montagnes string 
        /// </summary>
        /// <param name="montagnes">format : M-Horizontal-Vertical</param>
        public void PopulateMontagnes(List<string> montagnes)
        {
            foreach(string montagne in montagnes)
            {
                string[] splitline = montagne.Split('-');
                Coords coord = new Coords (splitline[1], splitline[2]);
                Montagnes.Add(new Montagne(coord));
            }
        }

        /// <summary>
        /// Populate the card with Tresors, given a list of Tresors string 
        /// </summary>
        /// <param name="montagnes">format : T-Horizontal-Vertical</param>
        public void PopulateTresor(List<string> tresors)
        {
            try
            {
                foreach (string tresor in tresors)
                {
                    string[] splitline = tresor.Split('-');
                    Coords coord = new Coords(splitline[1], splitline[2]);

                    if (!Int32.TryParse(splitline[3], out int tresorCount))
                        throw new FormatException("Cannot initialieze, tresor count is not numerical");

                    Tresors.Add(new Tresor(coord, tresorCount));

                }
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}

