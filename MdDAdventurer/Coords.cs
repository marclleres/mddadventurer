﻿using System;

namespace MdDAdventurer
{
    public class Coords
    {
        public int Horizontal { get; set; }
        public int Vertical { get; set; }

        public Coords()
        {
        }

        public Coords(int horizontal, int vertical)
        {
            Horizontal = horizontal;
            Vertical = vertical;
        }

        /// <summary>
        /// Constructor that parse String in entry and Throw exception if Entry is not numerical
        /// </summary>
        /// <param name="horizontal"></param>
        /// <param name="vertical"></param>
        public Coords(string horizontal, string vertical)
        {
            try
            {
                if (!int.TryParse(horizontal, out int hori) || !int.TryParse(vertical, out int vert))
                {
                    throw new FormatException("Your Coordonate is not numerical");
                }

                Horizontal = hori;
                Vertical = vert;
            }
            catch (Exception)
            {
                throw;
            }


        }

        /// <summary>
        /// Compare Instancied Coords with a given one
        /// </summary>
        /// <param name="coord"></param>
        /// <returns></returns>
        public bool Compare(Coords coord)
        {
            bool result = false;

            if(Horizontal == coord.Horizontal && Vertical == coord.Vertical)
            {
                result = true;
            }

            return result;
        }
    }
}
